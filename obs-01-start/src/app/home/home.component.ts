import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private firstObsSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    // this.firstObsSubscription = interval(1000).subscribe(
    //   count => {
    //     console.log(count);
    //   }
    // )

    // Create own dedicated custom  observable

    // const customIntervalObservable = Observable.create(obs => {
      const customIntervalObservable = new Observable(function subscribe(obs) {
      let count = 0;
      setInterval(() => {
        obs.next(count); // can be any input also e.g. 'counter: ' + count
        if (count > 12) {
          obs.error(new Error('Count is greater than 3'))
        }
        if (count === 10) {
          obs.complete();
        }
        count++;
      }, 1000);
    });



    this.firstObsSubscription = customIntervalObservable
      .pipe(
        filter(data => {
          return data > 5
        }),
        map((data:number) => {
          return 'Round: ' + (data + 1);
        }),
        map(data => {
          return 'More append: ' + data;
        })
      ).subscribe(data => {
      console.log(data);

    }, err => {
      console.log(err);
      alert(err.message);
    }, () => {
      console.log('Completed');
    });

    // this.firstObsSubscription = customIntervalObservable.subscribe(data => {
    //   console.log(data);

    // }, err => {
    //   console.log(err);
    //   alert(err.message);
    // }, () => {
    //   console.log('Completed');
    // });
  }

  ngOnDestroy() {
    console.log('Unsubscribe...');
    this.firstObsSubscription.unsubscribe();
  }
}

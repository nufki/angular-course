import { UserService } from './../user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  id: number;
  activatedSub: Subscription;
  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.activatedSub = this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
    });
  }

  onActivate(){
    console.log('onActivate() called.. subscribe');
    // this.userService.activatedEmitter.emit(true);   // emitter variant
    this.userService.activatedEmitter.next(true);   // emitter variant
  }

  ngOnDestroy(){
    console.log('ngOnDestroy() called.. unsubscribe');
    this.activatedSub.unsubscribe();
  }
}

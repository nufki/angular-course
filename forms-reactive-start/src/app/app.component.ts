import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;
  forbiddenUsernames = ['Chris', 'Emma'];

  ngOnInit() {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),  // Empty field with custom validator
        'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([])
    });

    // Obserables...
    // this.signupForm.valueChanges.subscribe(
    //   (value) => console.log(value)
    // );
    this.signupForm.statusChanges.subscribe(
      (stat) => console.log(stat)
    );

    // this.signupForm.setValue({
    //   'userData': {
    //     'username': 'knufer',
    //     'email': 'kei.nufer@gmail.com'
    //   },
    //   'gender': 'male',
    //   'hobbies': []
    // });

    this.signupForm.patchValue({
      'userData': {
        'username': 'knufer',
      }
    });
  }

  onSubmit(){
    console.log(this.signupForm);
    // this.signupForm.reset();
    this.signupForm.reset({
      'userData': {
        'username': 'knufer',
      }
    })

    // this.signupForm.value.userData.username;
  }

  onAddHobby(){
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }

  getHobbiesControls(){
    return (<FormArray>this.signupForm.get('hobbies')).controls;
  }

  //Return Syntax {nameIsForbidden: true}
  forbiddenNames(control: FormControl): {[s: string]: boolean } {
    if (this.forbiddenUsernames.indexOf(control.value) !== -1) {
      return {'nameIsForbidden': true}
    }
    return null; // Form Control is valid
  }

  forbiddenEmails (control: FormControl) : Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@test.com') {
          resolve({'emailIsForbidden': true});
        }
        else {
          return resolve(null);
        }
      }, 1500);
    });
    return promise;
  }
}

import { ServerResolver } from './servers/server/server-resolver.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { CanDeactivateGuard } from './servers/edit-server/can-deactivate-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { ServersComponent } from './servers/servers.component';
import { ServerComponent } from './servers/server/server.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EditServerComponent } from './servers/edit-server/edit-server.component';
import { NgModule } from "@angular/core";
import { AuthGuard } from './auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'users',
    component: UsersComponent,
    children: [
      {
        path: ':id/:name',  // Single User, the : indicate dynamic parameter
        component: UserComponent
      }
    ]
  },
  {
    path: 'servers',
    // canActivate: [
    //   AuthGuard
    // ],
    canActivateChild: [
      AuthGuard
    ],
    component: ServersComponent,
    children: [
      {
        path: ':id',
        component: ServerComponent,
        resolve: {server: ServerResolver}  // name (e.g. server) is up to you
      },
      {
        path: ':id/edit',
        component: EditServerComponent,
        canDeactivate: [
          CanDeactivateGuard
        ]
      }
    ]
  },
  // {
  //   path: 'not-found',
  //   component: PageNotFoundComponent
  // },
  {
    path: 'error-page',
    component: ErrorPageComponent,
    data: {message: 'Page not found!'}
  },
  {
    path: '**',
    redirectTo: '/error-page'    // make sure that this wild card path is the last as order matters
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})  // useHash is for old browser to tell the real server that everything behind # is application
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}

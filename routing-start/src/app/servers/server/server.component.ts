import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};

  constructor(private serversService: ServersService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    console.log ('id: ' + this.route.snapshot.params['id']);
    this.route.data
      .subscribe(
        (data: Data) => {
          this.server = data['server'];
        }
      );

    /*
    const id = +this.route.snapshot.params['id']; // + indicate convert to number.
    this.server = this.serversService.getServer(id);

    // Subscripe for parameter updates...
    this.route.params
    .subscribe(
      (params: Params) => {
        this.server = this.serversService.getServer(+params['id']);
      }
    );
    */
  }

  onEdit() {
    console.log(this.route.url);
    this.router.navigate(["edit"], {relativeTo: this.route, queryParamsHandling: 'preserve'});  // Apend edit
    // this.router.navigate(["/severs", this.server.id, 'edit']);  // Alternatively relative path
  }

}

import { Injectable } from "@angular/core";

// Not needed as not service is injected here
@Injectable({
  providedIn: 'root',
})
export class LoggingService{

  logStatusChanged(status: string){
    console.log('A server status changed, new status: ' + status);
  }
}

import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, limit: number, another: number) {
    console.log(another);
    if (value.length > 10) {
      return (<string>value).substring(0,limit) + ' ...';
    }
    return value;
  }

}

import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output() blueprintCreated  = new EventEmitter<{serverName: string, serverContent: string}>();

  //newServerName = '';    // only used in 2-way bindings (ngModel)
  //newServerContent = ''; // only used in 2-way bindings (ngModel)
  @ViewChild('serverContentInput', {static: true}) serverContentInput: ElementRef;  // Local Reference Solution
  
  constructor() { }

  ngOnInit(): void {
  }
  
  // Using Local References and not 2-way binding... 
  // IMPORTANT: 2 possiblities to use local references one via viewchild the other by using the parameter. 
  // The example below shows both... one by using a decorator (@viewchild) the other accessing the reference over parameter
  onAddServer(nameInput: HTMLInputElement) {
    console.log(this.serverContentInput);
  
    this.serverCreated.emit({
      serverName: nameInput.value, 
      serverContent: this.serverContentInput.nativeElement.value
    });

    // 2-way binding solution
    // console.log(serverNameInput.value);
    // this.serverCreated.emit({
    //   serverName: this.newServerName, 
    //   serverContent: this.newServerContent
    // });
  }

  onAddBlueprint(nameInput: HTMLInputElement) {
    console.log('Emit blueprint event to app component');
    this.blueprintCreated.emit({
      serverName: nameInput.value, 
      serverContent: this.serverContentInput.nativeElement.value
    });

    // 2-way binding solution
    // console.log(serverNameInput.value);
    // this.serverCreated.emit({
    //   serverName: this.newServerName, 
    //   serverContent: this.newServerContent
    // });    
  }
}

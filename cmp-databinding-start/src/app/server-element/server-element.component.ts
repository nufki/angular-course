import { Component, OnInit, Input, ViewEncapsulation, OnChanges, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, OnDestroy, ViewChild, ElementRef, ContentChild } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewInit, OnDestroy{

  @Input()element: {type: string, name: string, content: string};
  @ViewChild('heading', {static: true}) header: ElementRef;  
  @ContentChild('contentParagraph', {static: true}) paragraph: ElementRef;    

  constructor() { 
    console.log('Constructor Called');
  }

  ngOnInit(): void {
    console.log('ngOnInit Called');
    console.log('Text Content (ngOnInit)' + this.header.nativeElement.textContent);
    console.log('Text Content of paragraph (ngOnInit)' + this.paragraph.nativeElement.textContent);    
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges'); 
    console.log (changes);
  }

  ngDoCheck() {
    console.log('ngDoCheck Called');
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit Called');
    console.log('Text Content of paragraph (ngAfterContentInit)' + this.paragraph.nativeElement.textContent);
  }

  ngAfterContentChecked() {
    console.log('ngAfterContentChecked Called');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit Called');
    console.log('Text Content (ngAfterViewInit)' + this.header.nativeElement.textContent);
  }

  AfterContentChecked() {
    console.log('AfterContentChecked Called');
  }

  ngOnDestroy() {
    console.log('ngOnDestroy Called');
  }
}

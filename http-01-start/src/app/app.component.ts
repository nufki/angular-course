import { PostsService } from './posts.service';
import { Post } from './post.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: Post[] = [];
  isFetching:boolean = false;
  error = null;
  private errorSub: Subscription

  constructor(private http: HttpClient, private postService:PostsService) {}

  ngOnInit() {
    this.onFetchPosts();
  }

  onCreatePost(postData: Post) {
    console.log(postData);
    this.postService.createAndStorePost(postData.title, postData.content);
  }

  onFetchPosts() {
    // Subect Based Error Subscription
    this.errorSub = this.postService.error.subscribe(errorMessage => {
      this.error = errorMessage;
    });

    this.isFetching = true;
    this.postService.fetchPost().subscribe(
      respData => {
        this.isFetching = false;
        this.loadedPosts = respData;
      },
      error => {
        this.isFetching = false;
        this.error = error.message;
        console.log(error); // return back-end dependent error message
      }
    );
  }

  onClearPosts() {
    // Send Http request
    this.postService.clearPosts().subscribe(responseData => {
      this.loadedPosts = [];
    })
  }

  onHandleError(){
    this.error = null;
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }
}

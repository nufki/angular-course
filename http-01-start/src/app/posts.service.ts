import { map, catchError, tap } from 'rxjs/operators';
import { Post } from './post.model';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, throwError } from 'rxjs';


@Injectable({providedIn: 'root'})
export class PostsService{
  // in case multiple places interested in the error
  error = new Subject<string>();

  constructor(private http: HttpClient){
  }

  createAndStorePost(title: string, content: string) {
    const postData: Post = {title: title, content: content};

    this.http.post<{ name: string }>(
      'https://nufki-test.firebaseio.com/posts.json',
      postData,
      //  if full response needs to be evaluated e.g. the header
      // {
      //   observe: 'response'
      // }
    ).subscribe(responseData => {
      console.log(responseData);
    }, error => {
      this.error.next(error.message);
    });
  }

  fetchPost() {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print', 'pretty');
    searchParams = searchParams.append('custom', 'key');

    return this.http
    .get<{[key: string]: Post }>('https://nufki-test.firebaseio.com/posts.json',
      {
        headers: new HttpHeaders({'Custom-Header': 'hello'}),
        //params: new HttpParams().set('print', 'pretty')    // direct way...
        params: searchParams,
        // responseType: 'json' not necessary as we told the request to be json
      }
    )
    .pipe(
      map(data => {
      //map((data: {[key: string]: Post }) => {
        const postsArray: Post[] = [];
        for (const key in data) {
          if (data.hasOwnProperty) {
            postsArray.push({ ...data[key], id: key});
          }
        }
        return postsArray;
      }),
      catchError(errorResp => {
        // Generic Error Handling Stuff (logging, etc.), when done pass it forward...
        return throwError(errorResp);
      })
    );
  }

  clearPosts () {
    return this.http.delete(
      'https://nufki-test.firebaseio.com/posts.json',
      {
        observe: 'events',
        responseType: 'text'
      },
    ).pipe(
        tap(event =>  {
          console.log(event);
          if (event.type === HttpEventType.Sent) {
            console.log(event.type);
          }
          if (event.type === HttpEventType.Response) {
            console.log(event.body);
          }
        })
      );
  }
}

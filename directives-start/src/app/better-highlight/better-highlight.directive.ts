import { ElementRef, HostListener, HostBinding, Input } from '@angular/core';
import { Directive, Renderer2, OnInit } from '@angular/core';



// Accessing the element through the Renderer is better because: Angular is not limited running the browser... it also works with service workers
// Its simply the better way to access the DOM
// HostListener to react to any events (predefined such as mouseenter, mouseleave, etc.)
// When @Input is defining a alias the directive can be access direclty: [appBetterHighlight]="'red'" so typically only applied when having 1 binding only similar to ngStyle
// 4 types BINDING exits: Property Binding, Event Binding, 2-way Binding and String Interpolation
@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{
  // Allow binding from outside... e.g.
  @Input() defaultColor: string = 'transparent';
  @Input() highlightColor: string = 'blue';
  @HostBinding('style.backgroundColor') backgroundColor: string;

  constructor(private elRef: ElementRef, private renderer: Renderer2)  { }

  ngOnInit(){
    this.backgroundColor = this.defaultColor;
    //this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'blue');
  }

  @HostListener('mouseenter') mouseOver(eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'blue');     // Renderer Variant
    // this.backgroundColor = 'blue';
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') mouseLeave(eventData: Event){
    //this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'transparent'); // Renderer Variant
    this.backgroundColor = this.defaultColor;
  }
}

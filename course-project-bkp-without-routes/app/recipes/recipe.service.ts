import { ShoppingListService } from './../shopping-list/shopping-list.service';

import { EventEmitter, Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';


@Injectable({
  providedIn: 'root',
})
export class RecipeService{
  recipeSelected = new EventEmitter<Recipe>();

  constructor(private shoppingListService:ShoppingListService){}

  private recpies: Recipe[] = [
    new Recipe(
      'Wiener Schnitzel',
      'Echtes Wiener Schnitzel wird mit Kalbsschnitzel gemacht. Das Rezept für saftige, zarte Wiener Schnitzel gibt`s hier.',
      'https://cdn.gutekueche.de/upload/rezept/371/wiener-schnitzel.jpg',
      [
        new Ingredient('Fleisch', 1),
        new Ingredient('Pommes', 20),
        new Ingredient('Zitrone', 1),
      ]
    ),
    new Recipe(
      'Maki-Sushi mit Forellenfilet',
      'Sushi können Sie auch selber zubereiten - fast wie im japanischen Restaurant! Ein Versuch lohnt sich.',
      'https://www.bettybossi.ch/static/rezepte/x/bb_rkxx040501_0101a_x.jpg',
      [
        new Ingredient('Reis', 20),
        new Ingredient('Forelle', 1),
        new Ingredient('Nori', 4)
      ]
    )
  ];


  getRecipes() {
    return this.recpies.slice(); // other way to retrieve a copy of the array
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]){
    console.log('addIngredientsToShoppingList called');
    this.shoppingListService.addIngredients(ingredients);
  }
}
